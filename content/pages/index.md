Title: Edito
Author: ravages
Date: 12/04/2023
Slug: index
Status: hidden

Tu tiens dans tes mains le premier numéro d’une revue qui a failli s’appeler autrement. On avait pensé à Roue Libre, La Brèche, Le Pas-Sage, et même Le Blaireau Explosif. Finalement la revue s’appelle Ravages, avec un « s », parce qu’on est plusieurs à écrire là-dedans et surtout parce que des ravages y en a plein. Dans l’dico y’a écrit qu’un ravage est un dégât matériel causé de façon violente par l’action des gens ou de la nature. C’est aussi « l’effet désastreux de quelque chose sur quelqu’un », comme quand on parle des ravages de la guerre, ou de ceux du salariat.

Loin de s’imaginer comme des cataclysmes de chair et d’os qui répandraient la colère à l’aide de petites revues, l’idée est plutôt de témoigner des ravages de notre époque à partir d’un point d’observation précis, celui de la frontière franco-italienne à Briançon. On s’est dit que ça manquait un peu, dans le paysage militant du coin. Alors on a commencé à écrire. Certains de nos articles sont écrits à quatre, six, huit, parfois dix mains ! Et c’était pas toujours facile. Entre nous les critiques étaient vives, et certaines oreilles sourdes au moindre reproche[^1].

Pour le moment c’est tout !

Bonne lecture,

Textes : FleurBleu, KroustiKebs, Mody-Bic, Biche, Plume, Verveine Citronnée, Libé-nul, Daiyon.

Illustrations : Le dindon de la furss, Nao, vrrhngt, Plume, François, Léon.

[^1]: C’est pour rire...
