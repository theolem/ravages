Title: Soutenir
Author: ravages
Date: 12/04/2023
Slug: soutenir
Status: hidden

La revue Ravages est trouvable au format papier, à prix libre. Pour payer les premières impressions, nous avons cassé nos propres tirelires, plus celle du Collectif de Boulangères « Quarante Kilos et des Brioches ». Le coût d'impression unitaire est de 2€60.

Et, en parlant pognon, si tu veux nous aider financièrement, voici un IBAN : **FR76 1027 8090 7500 0204 1160 167**. En mettant RAVAGES dans l'intitulé du virement, ton virement ira dans notre caisse et servira à financer notre prochain numéro.

Si tu ne mets pas d'intitulé, il servira à d'autres collectifs politiquement proches de nos positions.
