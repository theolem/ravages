Title: Mentions légales
Author: ravages
Date: 12/04/2023
Slug: legals
Status: hidden

### Éditeur et directeur de la publication

Collectif Ravages
Pour nous contacter : revue.ravages@proton.me

### Informatique et libertés

#### Informations personnelles collectées

En France, les données personnelles sont notamment protégées par la loi n° 78-87 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l’article L. 226-13 du Code pénal et la Directive Européenne du 24 octobre 1995.

En tout état de cause le collectif Ravages ne collecte des informations personnelles relatives à l’utilisateur (nom, adresse
électronique, coordonnées téléphoniques) que pour le besoin des services proposés par ses sites, notamment pour
l’inscription à des espaces de discussion par le biais de formulaires en ligne ou pour des traitements statistiques. L’utilisateur fournit
ces informations en toute connaissance de cause, notamment lorsqu’il procède par lui-même à leur saisie. Il est alors précisé à
l’utilisateur de ces sites le caractère obligatoire ou non des informations qu’il serait amené à fournir.

#### Analyse statistique et confidentialité

En vue d’adapter le site aux demandes de nos visiteurs, nous analysons le trafic de nos sites sans cookies ou autre moyen intrusif.
Seul les requêtes faites sur chaque pages sont conservées pour des raisons légales et statistiques.

#### Rectification des informations nominatives collectées

Conformément aux dispositions de l’article 34 de la loi n° 48-87 du 6 janvier 1978, l’utilisateur dispose d’un droit de modification des
données nominatives collectées le concernant. Pour ce faire, l’utilisateur envoie à Jean-Cloud :

- Un courrier électronique en utilisant le formulaire de contact
- Un courrier à l’adresse du siège de l’association (indiquée ci-dessus) en indiquant son nom ou sa raison sociale, ses coordonnées
  physiques et/ou électroniques, ainsi que le cas échéant la référence dont il disposerait en tant qu’utilisateur d’un service hébergé ou
  publié par Jean-Cloud.

La modification interviendra dans des délais raisonnables à compter de la réception de la demande de l’utilisateur.

### Limitation de responsabilité

Ce site comporte des informations mises à disposition par des communautés ou sociétés externes ou des liens hypertextes vers d’autres sites qui n’ont pas été développés par le collectif Ravages. Le contenu mis à disposition sur le site est fourni à titre informatif. L’existence d’un lien de ce site vers un autre site ne constitue pas une validation de ce site ou de son contenu. Il appartient à l’internaute d’utiliser ces informations avec discernement et esprit critique. La responsabilité de l’
 ne saurait être engagée du fait des informations, opinions et recommandations formulées par des tiers.

Le collectif Ravages ne pourra être tenu responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site, et résultant soit de l’utilisation d’un matériel ne répondant pas aux spécifications techniques requises, soit de l’apparition d’un bug ou d’une incompatibilité.

Le collectif Ravages ne pourra également être tenu responsable des dommages indirects (tels par exemple qu’une perte de marché ou perte d’une chance) consécutifs à l’utilisation du site.

Des espaces interactifs (possibilité de poser des questions dans l’espace contact) sont à la disposition des utilisateurs sur le site du collectif Ravages . Le collectif Ravages se réserve le droit de supprimer, sans mise en demeure préalable, tout contenu déposé dans cet espace qui contreviendrait à la législation applicable en France, en particulier aux dispositions relatives à la protection des données. Le cas échéant, le collectif Ravages se réserve également la possibilité de mettre en cause la responsabilité civile et/ou pénale de l’utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamant, ou pornographique, quel que soit le support utilisé (texte, photographie…).

### Limitations contractuelles sur les données techniques

Le collectif Ravages ne pourra être tenue responsable de dommages matériels liés à l’utilisation du site. De plus, l’utilisateur du site s’engage à accéder au site en utilisant un matériel récent, ne contenant pas de virus et avec un navigateur de dernière génération mis-à-jour

### Propriété intellectuelle

Les polices utilisées sur ce site sont les suivantes :

* Century Schoolbook (https://www.freefonts.io/century-schoolbook-serif-font/)
* Erika Ormig (https://www.1001fonts.com/erika-ormig-font.html)

Tous les éléments graphiques ont été réalisés par les soins du collectif Ravages.
