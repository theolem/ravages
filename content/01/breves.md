Title: L'année 2023 vue d'ici
Author: ravages
Date: 12/04/2023
Weight: 0
Slug: breves
Special_CSS: breves2023

du début jusqu’à ce que l’on ait terminé de préparer ce numéro...

* _année islamique 1444,_
* _année hébraïque 5782, année du lapin dans le calendrier chinois,_
* _année des noms de chien·nes et de chat·tes en U,_
* _année d’un monde « plus en adéquation avec les valeurs humaines » selon les numérologues._

### 3 JANVIER
Entrée en vigueur, en Italie, d’un nouveau décret qui a pour objectif d’entraver les opérations de recherche et de sauvetage menées par les ONG en Méditerranée, rendant « encore plus dangereuse l’une des routes migratoires les plus meurtrières au monde » selon Médecins Sans Frontières.

### 18 JANVIER
C’est en qualité de président de la Communauté de Communes du Briançonnais, que Arnaud Murgia, maire de Briançon, procède à la fermeture de la MJC de Briançon, jugée
«trop autonome» en matière de gestion. Le cinéma et le centre social seront repris par l’intercommunalité. Un seul service est menacé de disparition : la MAPEmonde, Mission d’accueil des personnes étrangères.

### 10 FÉVRIER
Une bande de crétin.es s’amuse à taguer des drapeaux français dans le cimetière militaire de Briançon. La presse locale parle de profana- tion alors qu’aucune tombe n’a été touchée, et cela remonte jusqu’au Parisien. Arnaud Murgia accuse « les groupuscules de l’extrême gauche qui sévissent à la frontière ». Il pointe du doigt « les squats dans le centre- ville ». Dans les deux semaines suivantes, six personnes sans papiers, coupables seulement d’habiter dans un de ces squats, sont arrêtées pendant 24 heures pour une vérification d’identi- té, reçoivent des OQTF/ IRTF et sont assignées à résidence pendant 45 jours. Injustice est faite.

### 28 FÉVRIER
...et c’est en qualité de maire de Briançon, que Arnaud Murgia, président de la Communauté des Communes du Briançonnais, retire toutes subventions au collectif Saxifrage qui, depuis 2018, organisait le festival d’art vivant Chapeaux-Hauts. Certainement qu’il trouvait ce collectif trop autonome aussi.

### 1ER MARS
On comptabilise désormais deux nouveaux films mettant en scène la frontière franco-italienne : Les Engagés et Les Survivants. Des titres grandiloquents pour ... pas grand chose. Si votre meilleur·e ami·e veut aussi faire un film du même acabit, dites-lui d’arrêter tout de suite et proposez-lui plutôt d’aller manger une bonne glace.

### 15 MARS
Un maire de droite d’une petite ville de 13 000 habitants voit sa maison et ses voitures incendiées par un groupe d’extrême droite. La motivation du crime, le projet de construction d’un centre d’accueil de territoire de la commune. Voilà une chose qui n’arrivera pas à Briançon.

### 18 MARS
La Grande Maraude Solidaire, organisée comme tous les ans par un ensemble d’associations et de collectifs du coin, réunit 400 à 600 personnes devant les locaux de la Police de l’Air et des Frontières à Montgenèvre. La chorale chante et la fanfare fanfaronne, face à un rang compact de RoboCop impassibles.

### 25 MARS
À Sainte-Soline, d’autres RoboCop beaucoup moins impassibles dispersent à coups de lacrymo, grenades et flash- ball la foule qui manifestait contre un projet de méga-bassine. Nombreux·ses sont les blessé·es, dont plusieurs graves. Forte émotion partout chez les gens bien, à Briançon aussi.

### 11 AVRIL
L’Italie déclare l’état d’urgence migratoire pour les six mois à venir. Dans ce cadre, les réadmissions en Italie des personnes dublinées sont suspendues jusqu’à nouvel ordre. Que l’abominable règlement de Dublin soit mis à mal, ce ne peut être qu’une bonne nouvelle pour nous et pour la plupart de nos ami.es exilées. Juste on aurait préféré qu’il soit aboli par la gauche pour plein de bonnes raisons, plutôt que suspendu par l’extrême droite pour de très mauvaises.

### 10 MAI
Après plusieurs suspensions, renvois et reports, la loi asile-immigration est remise à l’ordre du jour par le gouvernement. Ça pue.

### 15 MAI
Arnaud Murgia, maire et président de fin bref z’avez compris, demande au gouvernement de déployer l’unité spéciale Border Force par chez nous aussi, comme à Menton. À croire qu’il n’aimerait pas les étranger·es çui-là.

### 21 MAI
le Refuge Solidaire de Briançon diffuse un communiqué de presse pour alerter la préfecture du dépassement de sa jauge d’accueil. C’est en fronçant très fort les sourcils qu’iels rappellent l’«État à ses responsabilités». Iels citent même «notre devise républicaine : Liberté – Égalité – Fraternité». La prochaine fois iels iront jusqu’à chanter la Marseillaise. Gare à toi monsieur le préfet !

### 9 JUIN
C’est avec un grand sourire aux lèvres qu’Arnaud Murgia et son copain Dominique Dufour, préfet des Hautes-Alpes, s’affichent à l’inauguration de l’Écrin 82-4000. Un centre de vacances à destination des personnes en situation de pauvreté... Il y a de quoi se demander ce qui les rend si heureux. Est-ce l’assurance que seules des personnes avec les bons papiers seront hébergées ici ? Ou peut-être rigolent-ils de s’afficher « solidaires » en sachant ce que Dominique allait annoncer le 13 juin ?...

### 13 JUIN
Si vertement rabroué par le shérif Murgia et par les légalistes bien-pensant·es du Refuge, l’État promptement intervient. À partirdu 24 mai, deux drones bourdonnent au-dessus de Montgenèvre. Et, pour le 1er juillet, on attend 150 policier·ères et gendarmes, en renfort pour « lutter contre l’immigration illégale entre l’Italie et la France ». C’est la Border Force, annoncée aujourd’hui par Dominique Dufour, préfet des Hautes-Alpes. Ça s’arrêtera quand ?

### 15 JUIN
Après l’insupportable nuit du 12 octobre 2021 où un train a percuté 4 personnes algériennes à Ciboure, tout près de la frontière franco-espagnole, on ne désespère pas de condamner l’État en justice. Une plainte a été déposée au tribunal judiciaire de Bayonne par l’unique survivant, les familles des victimes décédées et 3 associations nationales de défense des droits des personnes exilées. Les plaignant.e.s attendent notamment de cette information judiciaire qu’elle détermine le rôle causal des décisions prises pour la mise en œuvre de la politique de contrôle des frontières dans la survenance de ce drame.

### 17-18 JUIN
Les Soulèvements de la Terre appellent à manifester en Maurienne contre le projet de tunnel Turin-Lyon. La préfecture de Savoie interdit l’événement, mais plusieurs milliers de personnes se réunissent quand même. Attendues par deux-mille flics. Elle n’aura pas lieu, cette marche internationale qui devait ressembler à un torrent alpin.

### 27 JUIN
Nahel Merzouk est abattu par un flic à Nanterre pendant un contrôle d’identité. Plusieurs nuits d’émeute enflamment le pays. S’ensuivent 1500 interpellations, 448 incarcérations, un nombre imprécis de jeunes hommes tués et éborgnés par la police. Pendant ce temps, le racisme systémique, l’impunité des flics et l’insouciance des blancs perdurent (voire fleurissent).

### 4-6 AOÛT
Camping itinérant Passamontagna entre Claviere (Italie) et Briançon. La manifestation est organisée quasiment toutes les années depuis 2018. Mais cette année un déploiement inédit de flics réussira à empêcher le cortège de passer la frontière.

### 7 AOÛT
... et c’est au lendemain de cette manif’ avortée, au petit matin, qu’un homme est retrouvé mort sur un chemin de montagne entre la frontière et Briançon. Il s’appelait Moussa, était guinéen et n’avait pas les foutus BONS papiers.

### 30 AOÛT
La Belgique annonce qu’elle n’offrira plus d ’ h é b er g em en t aux hommes seuls demandant l’asile. C’était pourtant une obligation légale.

### 30 AOÛT
Après un mois d’août éreintant et des arrivées record dans les dernières nuits, les Terrasses Solidaires de Briançon ferment leurs portes. Pourtant la veille, au cours d’une grande réunion de crise, la proposition avait été avancée d’une belle collaboration entre le nouveau squat (le Pado), le Refuge et le diocèse (qui mettait à disposition une salle et un bout de terrain). Mais les propriétaires du Refuge n’ont rien voulu entendre. 300 personnes sont mises à la rue. Le bâtiment est fermé à l’aide de vigiles à chien et à sale tronche. Aucune date de réouverture est annoncée.

### 12 SEPTEMBRE
6000 personnes débarquent à Lampedusa en une seule journée. C’est un record.

### 19 SEPTEMBRE
La préfecture convoque 28 personnes d’un coup en leur faisant miroiter la régularisation. La plupart d’entre elles sont sur le territoire depuis plus de 5 ans, travaillent, ont des enfants scolarisés... Au guichet, la majorité d’entre elles se font confisquer leur passeport. Elles ressortent avec une obligation de quitter le territoire et 48h pour la contester.

### 25 SEPTEMBRE
Le propriétaire a fait couper l’eau et l’électricité au squat du Pado, dernier toit offert aux exilées à Briançon après la fermeture des Terrasses et de la paroisse Sainte-Catherine. Arnaud fait le tour des médias pour jouer les effarouchés. La police se met à contrôler les personnes qui ressemblent à des exilé·es en pleine rue à Briançon et à la gare. Les potes qui se font arrêter recoivent une OQTF, se font envoyer en centres de rétention administrative (CRA) ou sont reconduits en Italie. Coup dur pour le mythe du montagnard solidaire.
