AUTHOR = 'ravages'
SITENAME = 'ravages'
SITEURL = ''
SITESUBTITLE = 'chroniques de luttes à la frontière franco-italienne'

PATH = 'content'

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

MENU_CATEGORY = '01'

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

THEME = 'themes/ravages'

# Custom Jinja2 filter to get the Index page from Markdown
# From https://siongui.github.io/2016/02/19/pelican-generate-index-html-by-rst-or-md/
def pages_get_page_with_slug_index(pages):
  for page in pages:
      if page.slug == "index":
          return page
      else:
          print("nope")

JINJA_FILTERS = {
    "pages_get_page_with_slug_index": pages_get_page_with_slug_index,
}

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
